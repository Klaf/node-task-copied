# Node task

This project contains node hello-world

## Installation steps

1. Copy the repository
2. Install dependencies
3. Start the app

## Terminal commands

Code block: 

```sh
git clone https://gitlab.com/karrimiettinen/node-task
cd ./node-task
npm i
```

Start cmd: `npm run start`

## App should work

![alt text for test image](https://baap.ponet.fi/share/mosquitto_broker.png "a")

<!--![alt text](https://baap.ponet.fi/share/csgo/running-stickman-gif.gif "Logo Title Text 1")-->

<h2>TODO</h2>

- [x] Git ignore
- [ ] Software test
- [ ] Code comments
